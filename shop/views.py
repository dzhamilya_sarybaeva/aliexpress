from django.views.generic import DetailView, TemplateView
from shop.models import Category, Item, Slider


class CategoryView(DetailView):
    template_name = 'category.html'
    model = Category
    slug_field = 'slug'


class HomeView(TemplateView):
    template_name = 'home.html'

    def get_context_data(self, **kwargs):
        context = super(HomeView, self).get_context_data(*kwargs)
        context['slides'] = Slider.objects.all()
        context['categories'] = Category.objects.all()
        return context


class AboutView(TemplateView):
    template_name = 'about_us.html'


class ItemDetailView(DetailView):
    template_name = 'item_detail.html'
    model = Item
    slug_field = 'slug'
