from django.contrib import admin
from shop.models import Category, Item, Slider


class CategoryAdmin(admin.ModelAdmin):
    list_display = ['name', 'slug']


class ItemAdmin(admin.ModelAdmin):
    list_display = ['title', 'slug', 'description', 'price', 'category']


class SliderAdmin(admin.ModelAdmin):
    list_display = ['title', 'description', 'image']

admin.site.register(Category, CategoryAdmin)
admin.site.register(Item, ItemAdmin)
admin.site.register(Slider, SliderAdmin)
